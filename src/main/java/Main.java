import com.testinium.deviceinformation.helper.ProcessHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws InterruptedException, IOException {

        String testDeviceId = null;
//        String deviceId = testDeviceId;
//        String command = "dumpsys battery";

//        exec(deviceId, command);
//        androidADBtest();
        androidADBtest2();

    }

    public static void androidADBtest() throws InterruptedException, IOException {

        Process process = null;
        String commandString;
        String line;

        commandString = String.format("adb devices");

        System.out.print("Command is " + commandString + "\n");
        try {
            process = ProcessHelper.runTimeExec(commandString);
        } catch (IOException e) {
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

        while ((line = reader.readLine()) != null) {
            System.out.print(line + "\n");
        }

    }

    public static void androidADBtest2() throws InterruptedException, IOException {

        Process process = null;
        String commandString;
        String line;
        String ids;
        String[] heh;
        String regex = "\\b(?!device\\b)\\w+";

        commandString = String.format("adb devices");

        try {
            process = ProcessHelper.runTimeExec(commandString);
        } catch (IOException e) {
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

        //\b(?!device\b)\w+
        //TODO wywalić "Command is adb devices"
        //Testowane na: https://www.freeformatter.com/java-regex-tester.html#ad-output

//        ids = line.split(regex);
//
//        System.out.print(ids[0]);
//        System.out.printf(line);

//        while ((line = reader.readLine()) != null) {
//            System.out.print(line + "\n");
//        }

        // lookingAt() everything exept "device"
        // "\\b(?!device\\b)\\w+"

        while ((line = reader.readLine()) != null) {

            line = line.replaceAll("device", "");
            System.out.print(line + "\n");

            //TODO regex next word after X

            try {
                process = ProcessHelper.runTimeExec("adb -s " + line + " reboot");
            } catch (IOException e) {
            }

        }

    }

    // adb devices | tail -n +2 | cut -sf 1 | xargs -I X adb -s X shell dumpsys battery
    // adb devices | tail -n +2 | cut -sf 1


//    public static void exec(String deviceId, String command) throws IOException, InterruptedException {
//
//        Process process = null;
//        String commandString;
//        String line;
//
//        if(deviceId != null) {
//            commandString = String.format("%s", "adb -s" + deviceId + command);
//        }else
//            commandString = String.format("%s", "adb shell " + command);
//
//        System.out.print("Command is " + commandString + "\n");
//        try {
//            process = ProcessHelper.runTimeExec(commandString);
//        } catch (IOException e) {
//        }
//
//        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
//
//        while ((line = reader.readLine()) != null) {
//            System.out.print(line + "\n");
//
//        }
//
//    }

}